import hr
import employees
import productivity_system

salary_employee = employees.SalaryEmployee(1, 'Orest Romanovych', 12500)
hourly_employee = employees.HourlyEmployee(2, 'Marta Vasyleva', 40, 500)
commision_employee = employees.ComissionEmployee(3, 'Ivan Magadan', 15000, 1300)

payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll([
    salary_employee,
    hourly_employee,
    commision_employee
])
