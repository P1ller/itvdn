class Employee:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class SalaryEmployee(Employee):
    def __init__(self, id, name, weekly_salary):
        super().__init__(id, name)
        self.weekle_salary = weekly_salary

    def calculate_payroll(self):
        return self.weekle_salary


class HourlyEmployee(Employee):
    def __init__(self, id, name, hours_worked, hour_rate):
        super().__init__(id, name)
        self.hours_worked = hours_worked
        self.hour_rate = hour_rate

    def calculate_payroll(self):
        return self.hours_worked * self.hour_rate


class ComissionEmployee(SalaryEmployee):
    def __init__(self, id, name, weekly_salary, commission):
        super().__init__(id, name, weekly_salary)
        self.commission = commission

    def calculate_payroll(self):
        fixed = super().calculate_payroll()
        return fixed + self.commission

class Manager(SalaryEmployee):
    def work(self, hours):
        print(f'{self.name} screams and yells fo {hours}')

class Secretary(SalaryEmployee):
    def work(self, hours):
        print(f'{self.name} expends {hours} hours doing office paperwork')

class SalesPerson(ComissionEmployee):
    def work(self, hours):
        print(f'{self.name} expends {hours} hours of the phone')

class FactoryWorkers(HourlyEmployee):
    def work(self, hours):
        print(f'{self.name} manufactures gadjets for {hours} hours')
