def gen():
    num = 0
    while True:
        yield num
        num += 1


list_1 = [x ** 2 for x in range(10) if x % 2 == 0]
print(list_1)

gen_1 = (x**2 for x in range(10) if x % 2 == 0)
print((gen_1))

for i in gen_1:
    print(i)
