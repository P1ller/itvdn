from datetime import date, timedelta
today = date.today()


class DataRange:
    def __init__(self, start_date: date, stop_date: date, step: int = 1):
        self.start_date = start_date
        self.stop_date = stop_date
        self.step = step

    def __iter__(self):
        self.__current_date = self.start_date
        return self

    def __next__(self):
        if self.__current_date == self.stop_date:
            raise StopIteration
        self.__current_date += timedelta(days=self.step)
        return self.__current_date


start = date(2022, 2, 24)
stop = date.today()

for i in DataRange(start, stop):
    print(i)

print(list(DataRange(start, stop)))



