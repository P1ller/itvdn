number = input("Введіть число: ")

# Перевіряю щоб не більше 1 крапки, якщо є видаляю крапку, перевіряю складається чи з цифр.
if (number.count(".") <= 1) and number.replace(".", "").isdigit():

    number = float(number)
    if number % 2 == 0:
        print("Парне")
    else:
        print("Непарне")

else:
    print("Спрацював захист від дурня!")
