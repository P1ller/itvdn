num = input('Введіть ціле число: ')

if num.isdigit() or '-' in num:
    pos_or_neg = 'позитивне.' if int(num) > 0 else 'негативне.'
    if int(num) == 0:
        print('Число дорівнює 0')
    elif len(str(num)) == 1:
        print(f'Число однозначне та {pos_or_neg}')
    elif len(str(num)) == 2:
        print(f'Число двозначне та {pos_or_neg}')
    elif len(str(num)) == 3:
        print(f'Число трьохзначне та {pos_or_neg}')
    else:
        print(f'Число багатозначне та {pos_or_neg}')

else:
    print('Значення, яке Ви ввели, невірне. ВВЕДІТЬ ЦІЛЕ ЧИСЛО!')
