debugging = True
...


...
if debugging: print('Hello my function foo()')
foo()

x = int(input('Введіть температуру за вікном: '))

if x <= -15:
    print('Дуже холодно!')
elif -15 < x <= -5:
    print('Досить холодно :(')
elif -5 <= x <= 5:
    print('Слякоть')
elif x > 5 and x <= 15:
    print('Одягайте куртку!')
elif 15 < x < 25:
    print('Гарна погода...')
else:
    print('Спека...')