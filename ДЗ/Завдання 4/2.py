a = int(input('Введіть число: '))

fact = 1
for num in range(1, a + 1):
    fact = fact * num

print(f'Факторіалом числа {a} є {fact}')
