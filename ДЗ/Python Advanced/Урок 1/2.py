def get_sq(x, y):
    list_of_sq = []
    for num in range(x, y):
        if num % 2 != 0:
            list_of_sq.append(num * num)
    return list_of_sq


try:
    your_first_num = int(input("Enter first num of range: "))
    you_last_num = int(input("Enter last num of range: "))
    print(get_sq(your_first_num, you_last_num + 1))
except ValueError:
    print("You entered incorrect value!")
