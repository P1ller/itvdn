def even_decor(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        list_of_sq = []
        for _ in result:
            if _ % 2 == 0:
                list_of_sq.append(_)
        return list_of_sq
    return wrapper


@even_decor
def fibonacci(x):
    a, b = 0, 1
    for _ in range(x):
        yield a
        a, b = b, a + b


