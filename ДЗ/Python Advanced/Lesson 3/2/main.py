from xml.etree import ElementTree as ET
from faker import Faker
from pprint import pprint


fake_data = Faker("uk_UA")
persons = []

for _ in range(10):
    persons.append({"name": fake_data.name(),
                    "phone": fake_data.phone_number(),
                    "email": fake_data.ascii_email(),
                    "address": fake_data.address()})

pprint(persons)

personal_data = ET.Element('data')

for element in persons:
    record = ET.SubElement(personal_data, 'record')
    name = ET.SubElement(record, 'name')
    name.text = str(element['name'])
    address = ET.SubElement(record, 'address')
    address.text = str(element['address'])
    phones = ET.SubElement(record, 'phones')
    phone = ET.SubElement(phones, 'phone')
    phone.text = str(element['phone'])
    emails = ET.SubElement(record, 'emails')
    email = ET.SubElement(emails, 'email')
    email.text = str(element['email'])

tree = ET.ElementTree(personal_data)
tree.write('persons.xml', encoding='utf-8')


