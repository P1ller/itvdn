import json
from pprint import pprint


dict_1 = {
    "key_1": 1,
    "key_2": "adlkjkad",
    "key_3": True,
    "key_4": False,
    "key_5": 1456468,
    "key_6": "1",
    "key_7": 1.5,
}

dict_2 = {
    "key_a": 0,
    "key_b": 1,
    "key_c": 1,
    "key_d": 2,
    "key_e": 3,
    "key_f": 5,
    "key_g": 8,
}

json1 = json.dumps(dict_1)
json2 = json.dumps(dict_2)

with open("json_data", "a") as json_data:
    json_data.write(json1)
    json_data.write("\n")
    json_data.write(json2)


with open("json_data", "r") as json_file:
    json_format = json_file.readlines()


pprint(json_format)
