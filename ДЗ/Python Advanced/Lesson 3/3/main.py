import csv
from faker import Faker

fake = Faker("uk_UA")

with open('persons.csv', 'w', newline='', encoding="utf-8") as csvfile:
    fieldnames = ["name", "phone", "e-mail", "address"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, quoting=csv.QUOTE_ALL)
    writer.writeheader()
    for _ in range(10):
        writer.writerow({"name": fake.name(), "phone": fake.phone_number(), "e-mail": fake.ascii_email(),
                         "address": fake.address()})


with open("persons.csv", "r", encoding="utf-8") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f"Column names are: {', '.join(row)}")
            line_count += 1
        else:
            print(f"\t{row[0]} phone: {row[1]} e-mail: {row[2]} adress {row[3]}")
    print(f"Processed {line_count} lines")


with open('persons.csv', 'r', encoding="utf-8") as csvfile:
    persons = csv.DictReader(csvfile)
    persons_list = [person for person in persons]
