import json


def get_mcc(mcc):
    with open("mcc_codes.json", "r") as mcc_codes:
        data = json.load(mcc_codes)
    mcc_code = None
    for element in data:
        if element["mcc"] == mcc:
            mcc_code = element["combined_description"]
    return mcc_code


x = get_mcc("0742")
print(x)