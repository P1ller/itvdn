import main
from datetime import date
import csv
import json


def get_mcc(mcc):
    with open("mcc_codes.json", "r") as mcc_codes:
        data = json.load(mcc_codes)
    mcc_code = None
    for element in data:
        if element["mcc"] == mcc:
            mcc_code = element["combined_description"]
    return mcc_code


def start_programm():
    print("Hello!")
    while True:
        print("1. Add expense.\n"
              "2. Add expences from file. \n"
              "3. Add income.\n"
              "4. Show total expenses per month.\n"
              "5. Show total expenses ammount.\n"
              "6. Show income summ per month.\n"
              "7. Exit.")
        choice = input("Choose action: ")
        if choice == "1":
            t, s, mcc = input("Enter assignment: "), float(input("Enter ammount: ")), input("Enter MCC: ")
            main.execute_query(main.connect_to_db, main.insert_data.format(t, s, date.today(), get_mcc(mcc)))

        elif choice == "2":
            counter = 0
            with open("report-31-08-2022-22-20-02.csv", "r", encoding="utf-8") as file:
                data = csv.reader(file)
                next(data)
                for row in data:
                    if float(row[3]) < 0:
                        mcc = get_mcc(row[2])
                        counter += 1
                        main.execute_query(main.connect_to_db,
                                           main.insert_data.format(row[0], -float(row[3]), row[0], mcc))
            print(f"{counter} rows was added.")

        elif choice == "3":
            t, s = input("Enter source: "), float(input("Enter ammount: "))
            main.execute_query(main.connect_to_db, main.insert_data_income.format(t, s, date.today()))

        elif choice == "4":
            choose_month = input("Choose month (01 - 12): ")
            expences = main.execute_read_query(main.connect_to_db, main.get_expenses)
            sum_of_exp = []
            for exp in expences:
                if choose_month in exp[3]:
                    print(exp)
                    sum_of_exp.append(exp[2])
            print(f"Total expenses ammount is {float(sum(sum_of_exp))} UAH.")

        elif choice == "5":
            expences = main.execute_read_query(main.connect_to_db, main.get_expenses)
            sum_of_exp = []
            for exp in expences:
                sum_of_exp.append(exp[2])
            print(f"Total ammount of expences is {float(sum(sum_of_exp))} UAH.")

        elif choice == "6":
            choose_month = input("Choose month (01 - 12): ")
            income = main.execute_read_query(main.connect_to_db, main.get_income)
            sum_of_inc = []
            for inc in income:
                if choose_month in inc[3]:
                    print(inc)
                    sum_of_inc.append(inc[2])
            print(f"Total income is {float(sum(sum_of_inc))} UAH.")

        elif choice == "7":
            print("Have a nice day!")
            break
        else:
            print("You have entered incorrect value!")


if __name__ == "__main__":
    main.execute_query(main.connect_to_db, main.expences_table)
    main.execute_query(main.connect_to_db, main.income_table)
    start_programm()
