import sqlite3
from sqlite3 import Error


def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


expences_table = """
CREATE TABLE IF NOT EXISTS expences (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  assignment TEXT,
  ammount FLOAT,
  date TEXT,
  mcc TEXT
);
"""

income_table = """
CREATE TABLE IF NOT EXISTS income (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  source TEXT,
  ammount FLOAT,
  date TEXT
);
"""


def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        #print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")


connect_to_db = create_connection("./SQLiteDB")


insert_data = """
INSERT INTO
    expences (assignment, ammount, date, mcc)
VALUES
    ('{0}', {1}, '{2}', '{3}');
"""

insert_data_income = """
INSERT INTO
    income (source, ammount, date)
VALUES
    ('{0}', {1}, '{2}');
"""


def execute_read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as e:
        print(f"The error '{e}' occurred")


get_expenses = "SELECT * from expences"
get_income = "SELECT * from income"





