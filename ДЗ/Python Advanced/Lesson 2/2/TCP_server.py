import socket


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(("127.0.0.1", 12345))
sock.listen(5)

while True:
    try:
        cl, addr = sock.accept()
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        result = cl.recv(1024)
        print(f"Received nubmbers is: {result.decode('utf-8')}.")
        result_list = result.decode("utf-8").split(",")
        summ = str(sum([int(el) for el in result_list]))
        byte_summ = summ.encode("utf-8")
        cl.sendto(byte_summ, addr)
        cl.close()
