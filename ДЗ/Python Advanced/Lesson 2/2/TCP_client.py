import socket


def send_message():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("127.0.0.1", 12345))

        data = str(input("Enter your two numbers with ',' between: "))
        byte_data = data.encode("utf-8")

        sock.send(byte_data)
        result = sock.recv(64)
        print("Summ of your numbers is:", result.decode("utf-8"))
        sock.close()

    except ValueError:
        print("You have entered incorrect value! Process terminted. ")
