from TCP_client import send_message
import TCP_server

if __name__ == "__main__":
    while True:
        send_message()
        proceed = str(input("Would you like to proceed? (N to stop): "))
        if proceed == "N":
            TCP_server.sock.close()
            break

