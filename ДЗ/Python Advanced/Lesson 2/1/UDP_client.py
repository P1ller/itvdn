import socket


def send_message():
    try:
        data = str(input("Enter your IP: "))

        byte_bata = data.encode("utf-8")

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(byte_bata, ("127.0.0.1", 12346))

    except ValueError:
        print("You have entered incorrect value! Process terminted. ")
