from pprint import pprint

links_dict = {}
database_dict = {}


def execute(result):
    database = open("LINKS_DATABASE.txt", "r+")
    if result == "1":
        while True:
            link = input("Enter your link: ")
            alias = input("Enter alias: ")
            links_dict[alias] = link

            database.writelines(f"{alias} - {link}\n")

            proceed = input("Would You like to add more (N to stop)?: ")
            if proceed == "N":
                break

    elif result == "2":
        for key in links_dict:
            pprint(key)

    elif result == "3":
        while True:
            try:
                link_to_show = input("Enter alias: ")
                print(links_dict[link_to_show])
            except KeyError:
                print("There is no such key in dictionary.")
            proceed = input("Would You like see other link (N to stop)?: ")
            if proceed == "N":
                break

    elif result == "4":
        for line in database:
            stripped = line.strip("\n")
            clear = stripped.split("-")
            database_dict[clear[0]] = clear[1]
        for key in database_dict:
            pprint(key)

