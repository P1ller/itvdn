from main import execute


def run_program():
    name = input("Enter Your name: ")
    print(f"Hello, {name}!")
    while True:
        print("1. Add link.\n"
              "2. Show all aliases.\n"
              "3. Choose link to show.\n"
              "4. Show whole database.\n"
              "5. Exit.")
        choice = input(f"{name}, enter You choice: ")
        if choice in "1234":
            execute(choice)
        elif choice == "5":
            break
        else:
            print("Your choice is incorrect! Make proper choice!")

