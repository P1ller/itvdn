import get_primes


def run_program():
    while True:
        get_primes.execute()

        with open("primes.txt", "a") as data:
            data.writelines(str(get_primes.list_of_prime_numbers) + "\n")

        proceed = input("Want to check more? (N to stop): ")
        if proceed == "N":
            print("Have a nice day!")
            break
