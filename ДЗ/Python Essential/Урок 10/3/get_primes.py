def get_factors(num):
    """This function receives an positive integer and returns a list of it's factors"""
    factors_of_num = []
    for elem in range(1, num + 1):
        if num % elem == 0:
            factors_of_num.append(elem)

    return factors_of_num


list_of_prime_numbers = [1]


def execute():
    last_num = int(input("Enter your range: "))
    for n in range(2, last_num):
        if len(get_factors(n)) <= 2:
            list_of_prime_numbers.append(n)

    print(f"There is {len(list_of_prime_numbers)} prime numbers up to {last_num} "
          f"(including 1) - {list_of_prime_numbers}")
