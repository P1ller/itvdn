custom_dict = {
    "Orest": 1991,
    "Kolya": 1991,
    "Nazar": 1995,
    "Yura": 1994
}


def show_them(*args):
    for el in args:
        try:
            print(f"{el} = {custom_dict[el]}")
        except KeyError:
            print(f"There is no '{el}' in Dictionary!")


show_them("LOL", "Orest", "Kolya", "lalal")
