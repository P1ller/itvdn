string_1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"

string_2 = "Aliquam eu finibus nibh"

list_of_similar_letters = []

for letter in string_2:
    if letter in string_1 and letter != " ":
        list_of_similar_letters.append(letter)

print(list_of_similar_letters)
