# Создайте программу, которая эмулирует работу сервиса по сокращению ссылок. Должна быть реализована возможность
# ввода изначальной ссылки и короткого названия и получения изначальной ссылки по её названию.
from pprint import pprint


def execute():

    links_dict = {

    }

    while True:
        print("Hello!\n"
              "1. Add link.\n"
              "2. Show all aliases.\n"
              "3. Choose link to show.\n"
              "4. Exit. "
              )
        choice = input("Make yout choice: ")
        if choice == "1":
            while True:
                link = input("Enter your link: ")
                alias = input("Enter alias: ")
                links_dict[alias] = link
                proceed = input("Would You like to add more (N to stop)?: ")
                if proceed == "N":
                    break
        elif choice == "2":
            for key in links_dict:
                pprint(key)
        elif choice == "3":
            while True:
                link_to_show = input("Enter alias: ")
                print(links_dict[link_to_show])
                proceed = input("Would You like see other link (N to stop)?: ")
                if proceed == "N":
                    break
        elif choice == "4":
            break
        else:
            print("Your choice is incorrect! Make proper choice!")
