from pprint import pprint


def execute():
    database = open("LINKS_DATABASE.txt", "r+")
    links_dict = {}
    database_dict = {}
    print("Hello!")
    while True:
        print("1. Add link.\n"
              "2. Show all aliases.\n"
              "3. Choose link to show.\n"
              "4. Show whole database.\n"
              "5. Exit."
              )
        choice = input("Make yout choice: ")

        if choice == "1":
            while True:
                link = input("Enter your link: ")
                alias = input("Enter alias: ")
                links_dict[alias] = link

                database.writelines(f"{alias} - {link}\n")

                proceed = input("Would You like to add more (N to stop)?: ")
                if proceed == "N":
                    break

        elif choice == "2":
            for key in links_dict:
                pprint(key)

        elif choice == "3":
            while True:
                try:
                    link_to_show = input("Enter alias: ")
                    print(links_dict[link_to_show])
                except KeyError:
                    print("There is no such key in dictionary.")
                proceed = input("Would You like see other link (N to stop)?: ")
                if proceed == "N":
                    break

        elif choice == "4":
            for line in database:
                stripped = line.strip("\n")
                clear = stripped.split("-")
                database_dict[clear[0]] = clear[1]
            for key in database_dict:
                pprint(key)

        elif choice == "5":
            database.close()
            break

        else:
            print("Your choice is incorrect! Make proper choice!")
