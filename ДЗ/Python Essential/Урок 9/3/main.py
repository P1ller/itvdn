import pickle
import json

goods = {"Computers": ["video card", "memory", "screens"],
         "Toys": ["0 - 5 years", "6 - 12 years", "13 - 18"],
         "Smartphones": ["iPhone", "Samsung", "Xiaomi"]}

with open("DATA.pkl", "wb") as data:
    pickle.dump(goods, data)

with open("DATA2.json", "w") as data2:
    json.dump(goods, data2)
