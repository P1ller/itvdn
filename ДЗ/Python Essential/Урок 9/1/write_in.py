# Напишите скрипт, который создаёт текстовый файл и записывает в него 10000 случайных действительных чисел.
# Создайте ещё один скрипт, который читает числа из файла и выводит на экран их сумму.

from random import randint

with open("my_numbers.txt", "w") as file:
    for _ in range(10000):
        value = randint(0, 10000)
        file.writelines(str(value) + "\n")
        print(value)
