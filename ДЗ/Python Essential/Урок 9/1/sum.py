
with open("my_numbers.txt", "r") as file:
    numbers = [int(line) for line in file]

print(sum(numbers))
