class GraphicObject:
    def __init__(self, color):
        self.color = color


class Rectangle:
    def __init__(self, length, heigth):
        self.length = length
        self.heigth = heigth

    def get_square(self):
        print(f"Square of this rectangle is: {self.length*self.heigth}")


class ClickableObject:
    def __init__(self, movable):
        self.movable = movable

    def move(self):
        print("Clicked!")


class Button(GraphicObject, ClickableObject, Rectangle):
    def __init__(self, form, color, movable, length, heigth):
        GraphicObject.__init__(self, color)
        ClickableObject.__init__(self, movable)
        Rectangle.__init__(self, length, heigth)
        self.form = form


button_1 = Button("rectancgle", "red", True, 5, 4)
print(button_1.movable)
button_1.move()
button_1.get_square()

rectangle_1 = Rectangle(15, 7)
rectangle_1.get_square()

# Завдання 3

print(GraphicObject.__mro__)
print(Rectangle.__mro__)
print(Button.__mro__)
# в рядку 23 створений дочірній клас Button. Першим в порядку наслідування йде клас GraphicalObject. Відповідно,
# в першу чергу пошук методів здійснюватиметься там (після дочірнього класу). Якщо змінити місцями порядок
# наслідування, то зміниться і порядок пошуку.


