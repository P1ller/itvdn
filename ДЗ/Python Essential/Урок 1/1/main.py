import data


class Editor:
    def __init__(self, document):
        self.document = document

    def view_document(self):
        print("Opening document...")
        print(self.document)
        print("Document closed")

    @staticmethod
    def edit_document():
        print("This is standart version. Editing documents allowed only in ProEditor")


class ProEditor(Editor):
    def __init__(self, document):
        super().__init__(document)

    def __edit_document(self):
        print(self.document)
        change = input("Would You like to change Your document? (y/n): ")
        if change == "y":
            self.document = input("Enter changes: ")
            print(f"Your new document is: {self.document}")
        else:
            print("Nothing has changed")

    def call_edit_document(self):
        self.__edit_document()


def execute():
    """This function executes programm"""
    doc1 = Editor(data.document_1)
    doc1.view_document()
    doc1.edit_document()
    license_key = input("Enter your license key: ")
    if license_key in data.list_of_keys:
        doc1 = ProEditor(data.document_1)
        doc1.call_edit_document()
    else:
        print("Your key is incorrect!")
        doc1.edit_document()
