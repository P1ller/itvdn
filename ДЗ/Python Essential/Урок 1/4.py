# Создайте иерархию классов транспортных средств. В общем классе опишите общие для всех транспортных средств поля, в
# наследниках – специфичные для них. Создайте несколько экземпляров.
# Выведите информацию о каждом транспортном средстве.

class Transport:
    def __init__(self, mass, fuel, make, model):
        self.mass = mass
        self.fuel = fuel
        self.make = make
        self.model = model

    @staticmethod
    def move_forward():
        print("Moving forward")

    @staticmethod
    def move_backward():
        print('Moving backward')

    @staticmethod
    def turn_right():
        print("Turning right")

    @staticmethod
    def turn_left():
        print("Turning left")

    def __repr__(self):
        rep = f"Transport: {self.make}, {self.model}, {self.mass}, {self.fuel}"
        return rep


class TransportForPeople(Transport):
    def __init__(self, mass, fuel, make, model, passangers):
        super().__init__(mass, fuel, make, model)
        self.passangers = passangers

    def __repr__(self):
        rep = f"Transport: {self.make}, {self.model}, {self.mass}, {self.fuel}, {self.passangers}"
        return rep


class PublicTransport(TransportForPeople):
    def __init__(self, mass, make, model, passangers, electric_drive: bool, fuel):
        super().__init__(mass, fuel, make, model, passangers)
        self.electric_drive = electric_drive

    def __repr__(self):
        rep = f"Transport: {self.make}, {self.model}, {self.mass}, {self.fuel}, {self.passangers}, using electric drive"
        return rep


class CargoTransport(Transport):
    def __init__(self, mass, engine, make, model, load_carrying_capacity):
        super().__init__(mass, engine, make, model)
        self.load_carrying_capacity = load_carrying_capacity

    @staticmethod
    def open_cargo_trunk():
        print("Opening cargo trunk")

    def __repr__(self):
        rep = f"Transport: {self.make}, {self.model}, {self.mass}, {self.fuel}, {self.load_carrying_capacity}"
        return rep


transport_1 = Transport("2.8 t", 2.0, "BMW", "X3")
transport_2 = TransportForPeople("1.9 t", 1.6, "Mazda", "3", 4)
transport_3 = PublicTransport("40.0 t", "Elektron", "T5L64", 265, True, "electricity")
transport_4 = CargoTransport("12 - 18 t", 4.0, "Volvo", "FL", "16 t")
print(transport_1)
print(transport_2)
print(transport_3)
print(transport_4)
