def calculate():
    def get_summ(x, y):
        return x + y

    def get_quot(x, y):
        return x / y

    def get_prod(x, y):
        return x * y

    def get_dif(x, y):
        return x - y

    def get_power(x, y):
        return x ** y

    while True:
        operation = input("Choose operation (+, -, *, /, **): ")
        if operation in '*-+/**':
            try:
                first_num = float(input("Enter first number: "))
                second_num = float(input("Enter second number: "))
            except ValueError:
                print("Enter correct value!")
            else:
                if operation == '/':
                    try:
                        print(f"{first_num} / {second_num} = {get_quot(first_num, second_num)}")
                    except ZeroDivisionError:
                        print("You can't divide on '0'! ")
                elif operation == '*':
                    print(f"{first_num} * {second_num} = {get_prod(first_num, second_num)}")
                elif operation == '-':
                    print(f"{first_num} - {second_num} = {get_dif(first_num, second_num)}")
                elif operation == '+':
                    print(f"{first_num} + {second_num} = {get_summ(first_num, second_num)}")
                elif operation == '**':
                    try:
                        print(f"{first_num} ** {second_num} = {get_power(first_num, second_num)}")
                    except ZeroDivisionError:
                        print("You can't raise '0' to a negative number!")
        else:
            print("Incorrect operation")
        proceed_or_not = (input("Would You like to caclulate more (Y / N): "))
        if proceed_or_not == "N":
            break


calculate()
