# Напишите рекурсивную функцию, которая вычисляет сумму натуральных чисел, которые входят в заданный промежуток.

def calc_summ_in_range(x):
    print(sum(range(0, x)))
    if x == 0:
        return
    else:
        calc_summ_in_range(x - 1)


calc_summ_in_range(6)
