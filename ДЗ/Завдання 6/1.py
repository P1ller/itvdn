print(abs(1 + 2j))

print(all([1, 3, 'akdjhka', 5.5]))

print(any([1, 3, 'akdjhka', 5.5]))
print(any([]))

print(ascii(ord('5')))

print(repr([1, 5]))

print(bin(2554))

print(bool([5]))


class Human:
    pass

print(callable(Human))

print(chr(97))


class MyObj(object):
    def __init__(self):
        self.name = 'Chuck Norris'
        self.phone = '+6661'


obj = MyObj()
print(obj.__dict__)
print(dir(obj))

delattr(obj, 'name')
print(dir(obj))

print(divmod(5, 7))

list1 = ['Січень', 'Лютий', 'Березень', 'Квітень']
print(list(enumerate(list1)))

sd = 1
print(eval('sd + 5'))

print(float('-15'))

print(frozenset(list1))

print(getattr(MyObj, '__class__'))

print(globals())

print(hasattr(MyObj, '__class__'))

print(hash(MyObj))

# print(help(list1)) закоментував, бо дуже багато виводиться тексту

print(hex(15))

print(id(list1))

# print(input('Введіть щось: ')

print(int())

print(isinstance(list1, list))

print(issubclass(MyObj, int))

print(iter(list1))

print(len(list1))

smth = ('fgfg', 'adad', None, False, 55, 7)
print(list(smth))

print(locals())

list_2 = [-2, -1, 0, 1, 2]
abs_of_list = list(map(abs, list_2))
print(abs_of_list)

print(max(list_2))

print(min(list1))

byte_dude = b'lalala'
print(memoryview(byte_dude))

print(next(iter(list_2)))

print(oct(5))

print(ord('a'))

print(pow(5, 2))

for num in range(5):
    print(num)

for num in range(1, 8, 2):
    print('Hello')

print(reversed(list1))

print(round(5.5645153, 2))

print(set(list1))

print(list1[1:3:1]) # slice

print(str(5))

print(sum(list_2))

class First:
    def __init__(self, legs, hands):
        self.legs = legs
        self.hands = hands

class Second(First):
    def __init__(self, legs, hands, name):
        super().__init__(legs, hands)
        self.name = name

print(tuple(list1))

print(type(list1))

print(vars(First))

for item in zip(list_2, list1):
    print(item)
