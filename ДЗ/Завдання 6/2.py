def is_palindrome(phrase=input('Enter phrase: ')):
    if phrase == phrase[::-1]:
        print(f"Phrase '{phrase}' is a palindrome. ")
    else:
        print(f"Phrase '{phrase}' is not a palindrome")

# як варіант можна зробити такий, але перший варіант мені подобається візуально більше:

is_palindrome()

def is_palindrome_2(phrase=input('Enter phrase: ')):
    if phrase == phrase[::-1]:
        return True
    else:
        return False

print(is_palindrome_2())
