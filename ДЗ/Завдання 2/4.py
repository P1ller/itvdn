import math

pi_number = math.pi
rad = float(input('Введіть радіус круга, см: '))

cirkle_sq = pi_number * (rad ** 2)
print(f'Площа круга становить: {cirkle_sq.__round__(2)} сантиметрів.')
