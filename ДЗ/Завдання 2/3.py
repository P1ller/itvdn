# Напишите программу, которая решает квадратное уравнение  по формулам . Значения a, b и c вводятся с клавиатуры.
# Для извлечения корня используйте оператор возведения в степень, а не функцию math.sqrt,
# чтобы получить комплексные числа в случае, если подкоренное выражение отрицательно.

a = int(input('Введіть число a: '))
b = int(input('Введіть число b: '))
c = int(input('Введіть число c: '))

discriminant = (b ** 2) - (4 * a * c)

root_from_discriminant = discriminant ** 0.5

x1 = (-b - root_from_discriminant) / 2 * a
print(x1)

x2 = (-b + root_from_discriminant) / 2 * a
print(x2)

print(f'Результати такі: \n'
      f'x1: {a * (x1 ** 2)} + {b * x1} + {c} = 0 \n'
      f'x2: {a * (x2 ** 2)} + {b * x2} + {c} = 0')