import math

a = int(input('Введіть значення a: '))
actions_tuple = ('sin', 'cos', 'tan', '+', '-', '*', '/')
action = input(f'Введіть одну з дій {actions_tuple}: ')
if action in actions_tuple:
    if action == 'sin':
        result = math.sin(a)
        print(f'{action.capitalize()} {a} становить: {result}')
    elif action == 'cos':
        result = math.cos(a)
        print(f'{action.capitalize()} {a} становить: {result}')
    elif action == 'tan':
        result = math.tan(a)
        print(f'{action.capitalize()} {a} становить: {result}')
    else:
        if action == '+' or '-' or '/' or '*':
            b = int(input('Введіть значення b: '))
            if action == '+':
                result = a + b
            elif action == '-':
                result = a - b
            elif action == '*':
                result = a * b
            elif action == '/':
                result = a / b
        print(f'Результат: {a} {action} {b} = {result}')
else:
    print('Калькулятор не підтримує введену Вами дію. Я поки лише вчусь :)')