a = int(input('Введіть число a: '))
b = int(input('Введіть число b: '))
c = int(input('Введіть число c: '))

discriminant = (b ** 2) - (4 * a * c)
print(f'Дискримінант дорівнює {discriminant}')

if discriminant < 0:
      print('Дійсні корені - відсутні')
elif discriminant == 0:
      x = -(b / 2 * a)
      print(f'Результат такий: \n'
            f'{a * (x ** 2)} + {b * x} + {c} = 0')
else:
      root_from_discriminant = discriminant ** 0.5
      x1 = (-b - root_from_discriminant) / 2 * a
      # print(x1)
      x2 = (-b + root_from_discriminant) / 2 * a
      # print(x2)
      print(f'Результати такі: \n'
            f'x1: {a * (x1 ** 2)} + {b * x1} + {c} = 0 \n'
            f'x2: {a * (x2 ** 2)} + {b * x2} + {c} = 0')
