for elem in range(-10, 11, 1):
    x = elem
    if -5 <= x <= 5:
        y = x ** 2
    elif x < -5:
        y = 2 * x - 1
    elif x > 5:
        y = 2 * x
    print(f'X = {x}. Y = {y}')
