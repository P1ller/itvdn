word = "Elements are subtracted from an iterable or from another mapping (or counter)"

counter = {}

for letter in word:
    if letter not in counter:
        counter[letter] = 0
    counter[letter] += 1

print(counter)

from collections import defaultdict

counter_1 = defaultdict(int)

for letter in word:
    counter_1[letter] += 1

print(counter_1)

from collections import Counter
a = Counter(word)

print(a)