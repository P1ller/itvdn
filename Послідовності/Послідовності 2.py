from collections import OrderedDict

dict_1 = OrderedDict()

dict_1["dog"] = "Sharik"
dict_1["cat"] = "Murka"
dict_1["cow"] = "Zirka"

for key, value in dict_1.items():
    print(f"{key} --> {value}")

dict_2 = OrderedDict()

dict_2["dog"] = "Sharik"
dict_2["cat"] = "Murka"
dict_2["cow"] = "Zirka"

print(dict_2 == dict_1)

