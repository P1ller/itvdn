from collections import namedtuple
from collections import deque

# namedtuple

def custom_divmod(x, y):
    DivMode = namedtuple("Divmod", "quotient remainder")
    return DivMode(*divmod(x, y))

Employee = namedtuple("Employee", "name age position salary")

a = Employee("Adam Smith", 35, "data engineer", 2000)
print(a.name)

ticket_queue = deque()

ticket_queue.append("Orest")
ticket_queue.append("Ivan")
ticket_queue.append("Roman")
ticket_queue.append("Olesya")

print(ticket_queue)
left1 = ticket_queue.popleft()

ticket_queue_2 = deque("qwerty", 7)
print(ticket_queue_2)