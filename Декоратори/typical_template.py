import functools

def decorator(func):
    @functools.wraps(func)
