def my_decorator(func):
    def wrapper():
        print("before func called")
        func()
        print("after the func is called")
    return wrapper

def say_whee():
    print("Whee!")

say = my_decorator(say_whee)


say()