def parent(num):
    def first_child():
        return "Hi! I'a a Stepan"

    def second_child():
        return "Call me Olga"

    if num == 1:
        return first_child
    else:
        return second_child


first = parent(1)
second = parent(2)