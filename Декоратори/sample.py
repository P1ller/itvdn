import functools

class MyException1(Exception):
    pass

class MyException2(Exception):
    pass

class MyException3(Exception):
    pass


def my_flag_worker(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            res = func(*args, **kwargs):
            return res
        except MyException1

def func_1():
    a = input("----->")
    if a.isdigit():
        raise MyException1
    elif a.isalpha():
        raise MyException2
    else:
        raise MyException3