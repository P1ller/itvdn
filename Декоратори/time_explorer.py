import functools
import time

def timer(func):
    """Print the runtime of th decorator"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

@timer
def ffff(num_times):
    for _ in range(num_times):
        sum([i**5 for i in range(10000)])

if __name__ == "__main__":
    ffff(5000)