def say_hello(name=input("Введіть своє ім'я: ")):
    if name != 'Орест':
        print(f'Привіт, {name}!')
    else:
        print('Привіт, Орест!')

say_hello()
