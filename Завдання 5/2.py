def first(num):
    if num > 0:
        return num * 5
    else:
        return num * 2

def second():
    num2 = -5
    while num2 <= 5:
        num3 = first(num2)
        print(f'first({num2}) = {num3}')
        num2 += 0.5

second()
