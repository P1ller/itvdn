def f():
    print('I am an f-function')
f()

def we_dont_need_to_do_nothing():
    pass

def sum(a, b):
    print(a + b)

sum(10, 5)
sum('aaaaaaaaaaaaaaaaaaa', 'ffffffffffffffffff')

def pretty_print(qty, name, price):
    print(f'{qty} {name} cost ${price:.2f}')

pretty_print(2, 'banana', 1)

def checkbox(qty, item, price=1.256):
    print(f"{qty} {item}s costs ${(price * qty):.2f} (${price:.2f} per 1 {item})")

checkbox(5, 'banana')
checkbox(6, 'cherry')
checkbox(7, 'ck', price=1)

def f_var(my_list=None):
    my_list.append('###')
    return my_list


def f_var2(my_list):
    my_list.append('###')
    return my_list


def ggg(*args): # пакування кортежу у функції
    print(args)
    print(type(args), ' ', len(args))
    for i in args:
        print(i)

ggg(1, 4, 'gg', 1.17, 'ttt')

def hhh(x, y, z):
    print(f'x = {x}')
    print(f'y = {y}')
    print(f'z = {z}')

hhh(1, 2, 3)
tt = (7, 8, 9)
hhh(*tt) # розпакування кортежу і всіх ітерабельних обєктів

def key_key(**kwargs): #keywords arguments
    print(kwargs)
    print(type(kwargs))
    for key, value in kwargs.items():
        print(f'{key}: {value}')

key_key(one=1, two=2, three=3)

a = {
    'la': 2,
    'ada': 3,
    'adad': 4,
}

key_key(**a)

def kkk(a, b, *args, **kwargs):
    print(f'a = {a}, b = {b}')
    print(f'args = {args}')
    print(f'kwargs = {kwargs}')

kkk(1, 2)
kkk(2, 3, fff=15, kkkkkk=67)
kkk(2,3, 4, 5, ggf=1, aaaaaaaaaa=48)
