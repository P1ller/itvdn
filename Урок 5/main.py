list_9 = [1, 'sss', [2, 3.5, 'sjkfh', ['aa', 'bb', 100]], 35, 67, 100.001, 'zzz']
print(list_9[0])
print(list_9[1])
print(list_9[1][2])
print(list_9[2][2][3])

list_10 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(list_10[2:50])
print(2 in list_10)
print(22 in list_10)
print([0] + list_10)
print(list_10 * 3)
print(min(list_10))
print(max(list_10))
del list_10[0]
print(list_10)
print(list_10[2:5])
tuple_1 = (1, 2, 3, 4)
print(tuple_1)
tuple_2 = 1, 2, 3, 4
print(tuple_2)
tuple_3 = (3,)
tuple_4 = 4,
print(type(tuple_4), type(tuple_3))
t = ('foo', 'bar', 'baz', 'qux')
# пакування кортежа
a = 'aa'
b = 'bb'
c = 'cc'
tuple_x = a, b, c
print(tuple_x)
# розпакування
a1, b1, c1 = tuple_x
print(a1, b1, c1)

a3, b3, c3 = (1, 2, 3)
