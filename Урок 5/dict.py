dict_1 = {
    'key_1': 1,
    'key_2': 2,
    3: 3,
    555: 'rrrrr',
    (1, 2, 4): [4, 4, 4]
}

for i in dict_1:
    print(i)


for i in dict_1.values():
    print(i)

for key, value in dict_1.items():
    print(f'{key}: {value}')