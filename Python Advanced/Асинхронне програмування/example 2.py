# 2 potoky

import threading
import time


def handler(started=0, finished=0):
    result = 0
    for i in range(started, finished):
        result += 1
    results.append(result)


results = []

task1 = threading.Thread(
    target=handler,
    kwargs={'finished': 2 ** 14}
)

task2 = threading.Thread(
    target=handler,
    kwargs={'started': 2 ** 14, 'finished': 2 ** 28}
)

started_at = time.time()

task1.start()
task2.start()

task1.join()
task2.join()

print('RESULTS 1')
print('Time: {}'.format(time.time() - started_at))
print('Value:', sum(results))

results = []
started_at = time.time()
handler(finished=2 ** 28)
print('RESULTS 2')
print('Time: {}'.format(time.time() - started_at))
print('Value:', sum(results))
