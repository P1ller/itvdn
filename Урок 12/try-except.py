import sys

class MyException(Exception):
    pass

def linux_interation():
    assert ('linux' in sys.platform), 'Function can run only on Linux systems.'
    print('Doing smth')

try:
    linux_interation()
    x = float(input('Enter number: '))
    if x < 0:
        raise MyException('x is less than 0')
    a = 10 / x
except AssertionError as error:
    print(error)
    print('in exception block 1')
except ZeroDivisionError as error:
    print(error)
    print('in exception block 2')
except MyException as error:
    print(error)
    print('in exception block 3')
except BaseException as error:
    print(error)
    print('in exception block 4')
else:
    print('I am in module else')
finally:
    print('I will be here anyway')